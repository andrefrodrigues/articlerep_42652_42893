package cp.articlerep.ds;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import cp.articlerep.MainRep;

/**
 * @author Ricardo Dias
 */

/*
 * Invariants by us:
 * 
 * (count >=0) AND (removed<=added) AND (added-removed = count)
 * 
 */
public class HashTable<K extends Comparable<K>, V> implements Map<K, V> {

	private static class Node {
		public Object key;
		public Object value;
		public Node next;

		public Node(Object key, Object value, Node next) {
			this.key = key;
			this.value = value;
			this.next = next;
		}
	}

	private Node[] table;
	private Lock[] locks;
	
	// hash table validation
	private AtomicInteger added;
	private AtomicInteger removed;
	private AtomicInteger count;

	public HashTable() {
		this(1000);
	}

	public HashTable(int size) {
		this.table = new Node[size];
		if(MainRep.DO_VALIDATION) {
			added = new AtomicInteger();
			removed = new AtomicInteger();
			count = new AtomicInteger();
		}
		this.locks = new Lock[size];

		for (int i = 0; i < size; i++)
			this.locks[i] = new ReentrantLock();
	}

	private int calcTablePos(K key) {
		return Math.abs(key.hashCode()) % this.table.length;
	}

	@SuppressWarnings("unchecked")
	@Override
	public V put(K key, V value) {
		int pos = this.calcTablePos(key);

		try {
			locks[pos].lock();
			Node n = this.table[pos];

			while (n != null && !n.key.equals(key)) {
				n = n.next;
			}

			if (n != null) {
				V oldValue = (V) n.value;
				n.value = value;
				return oldValue;
			}

			Node nn = new Node(key, value, this.table[pos]);
			this.table[pos] = nn;
			if(MainRep.DO_VALIDATION) {
				this.count.incrementAndGet();
				this.added.incrementAndGet();
			}
			return null;

		} finally {
			locks[pos].unlock();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public V remove(K key) {
		int pos = this.calcTablePos(key);
		try {
			locks[pos].lock();
			Node p = this.table[pos];
			if (p == null) {
				return null;
			}

			if (p.key.equals(key)) {
				this.table[pos] = p.next;
				if(MainRep.DO_VALIDATION) {
					this.removed.incrementAndGet();
					this.count.decrementAndGet();
				}
				return (V) p.value;
			}

			Node n = p.next;
			while (n != null && !n.key.equals(key)) {
				p = n;
				n = n.next;
			}

			if (n == null) {
				return null;
			}

			p.next = n.next;
			if(MainRep.DO_VALIDATION) {
				this.removed.incrementAndGet();
				this.count.decrementAndGet();
			}
			return (V) n.value;
		} finally {
			locks[pos].unlock();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public V get(K key) {
		int pos = this.calcTablePos(key);
		try {
			locks[pos].lock();
			Node n = this.table[pos];
			while (n != null && !n.key.equals(key)) {
				n = n.next;
			}
			return (V) (n != null ? n.value : null);
		} finally {
			locks[pos].unlock();
		}
	}

	@Override
	public boolean contains(K key) {
		return get(key) != null;
	}

	/**
	 * No need to protect this method from concurrent interactions
	 */
	@Override
	public Iterator<V> values() {
		return new Iterator<V>() {

			private int pos = -1;
			private Node nextBucket = advanceToNextBucket();

			private Node advanceToNextBucket() {
				pos++;
				while (pos < HashTable.this.table.length && HashTable.this.table[pos] == null) {
					pos++;
				}
				if (pos < HashTable.this.table.length)
					return HashTable.this.table[pos];

				return null;
			}

			@Override
			public boolean hasNext() {
				return nextBucket != null;
			}

			@SuppressWarnings("unchecked")
			@Override
			public V next() {
				V result = (V) nextBucket.value;

				nextBucket = nextBucket.next != null ? nextBucket.next : advanceToNextBucket();

				return result;
			}

		};
	}

	@Override
	public Iterator<K> keys() {
		return null;
	}

	@Override
	public boolean validate() {

		if (this.count.get() < 0) {
			System.out.println("Count < 0");
			return false;
		}

		int realElements = countElements();
		if (realElements != this.count.get()) {
			System.out.println("Count is different of real element count");
			return false;
		}

		if (this.removed.get() > this.added.get()) {
			System.out.println("More items removed than added...");
			return false;
		}

		if (this.count.get() != this.added.get() - this.removed.get()) {
			System.out.println("Count != ADDED - REMOVED");
			return false;
		}

		return true;
	}

	private int countElements() {
		Iterator<V> values = this.values();
		int count = 0;
		while (values.hasNext()) {
			count++;
			values.next();
		}
		return count;

	}

}
